dvtm (0.15+40.g311a8c0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * QA Upload.
    Orphan package - see bug 983796.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 13.
  * Update standards version to 4.6.1, no changes needed.

  [ Matheus Polkorny ]
  * debian/control:
    - Build-depend libncurses5w-dev removed.
    - Build-depend on libncurses-dev, libncurses5-dev is deprecated.
    - Bumped Standards-Version to 4.7.0.
  * debian/patches:
    - Added description in fix-spelling-errors.patch.
    - Added Forwarded field increase-number-of-tags-to-9-to-use-all-.patch.
    - Forwarded to Upstream restore-signal-handlers.patch.
  * debian/upstream: Add metadata in package.
  * debian/watch:
    - Bumped version to 4.
    - Updated watch URL to HTTPS.

 -- Matheus Polkorny <mpolkorny@gmail.com>  Sat, 25 May 2024 21:40:00 -0300

dvtm (0.15+40.g311a8c0-1) unstable; urgency=medium

  * New snapshot of upstream repository (Closes: #917568)
  * Refresh patches
    + Drop `dvtm-editor.patch', since upstream now includes
      code to use any text editor for copy mode.
  * Extract current package version in Makefile from debian/changelog
    instead of invoking git.
  * Update standards version to 4.3.0 (no changes needed)
  * Enable large file support
  * Fix spelling error in manpage

 -- Dmitry Bogatov <KAction@debian.org>  Thu, 03 Jan 2019 09:59:26 +0000

dvtm (0.15-5) unstable; urgency=medium

  * Increase number of tags to 9, to use all keyboard and match
    default configuration of `dwm'.

 -- Dmitry Bogatov <KAction@debian.org>  Fri, 28 Dec 2018 03:39:45 +0000

dvtm (0.15-4) unstable; urgency=medium

  * Fix typo in package description (s/it's/its)
  * Build-depend on `debhelper-compat' (obsoletes `debian/compat')
  * Bump standards version to 4.2.1 (no changes needed)
  * Update Vcs-* fields in debian/control.

 -- Dmitry Bogatov <KAction@debian.org>  Sat, 24 Nov 2018 21:34:36 +0000

dvtm (0.15-3) unstable; urgency=medium

  * Add support for rootless build
  * Update standards version to 4.1.4 (no changes needed)
  * Add Vcs-* fields into debian/control
  * Replace deprecated priority 'extra' with 'optional'
  * Bump debhelper compat version to 11 (no changes needed)
  * Use secure protocol when referering to copyright format file
  * Remove unused lintian override
  * Do not install terminal description; depend on ncurses-term
    instead. (Closes: #897953)

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 11 May 2018 19:38:43 +0300

dvtm (0.15-2) unstable; urgency=medium

  * debian/patches/restore-signal-handlers.patch: Restore default signal
    handlers for child processes. See #841090 for detailed discussion.
    + Thanks: Ian Jackson <ijackson@chiark.greenend.org.uk>
  * patches/highlight-selected-tag.patch: Make selected tag in bar more
    contrast.

 -- Dmitry Bogatov <KAction@gnu.org>  Thu, 01 Dec 2016 15:20:12 +0300

dvtm (0.15-1) unstable; urgency=medium

  * New maintainer (Closes: #824284, #825269)
  * New upstream release (Closes: #793472)
  * Bump debhelper compat level (9 -> 10), and add lintian override
    about wrong versioned debhelper dependency
  * Replace patch about stripping and PREFIX with environment variables,
    exported from debian/rules.
  * Adjust debian/docs to match upstream, renamed README to README.md
  * Convert debian/copyright into DEP-5 format
  * Remove `config.h' at clean stage
  * Bump standards version to 3.9.8 (no changes needed)
  * Enable hardening
  * Make copymode work out-of-box in most cases
  * Export every available command for fifo scripting

 -- Dmitry Bogatov <KAction@gnu.org>  Thu, 28 Apr 2016 03:39:11 +0300

dvtm (0.12-1) unstable; urgency=medium

  * New upstream release (Closes: #760607)
  * Switch to debhelper compat level 9
  * cflags_and_strip_fixes.diff: refresh following upstream changes
  * debian/control:
    - Update Standards-Version to 3.9.5, no change required
    - Update email address from albin.tonnerre@gmail.com to lutin@debian.org

 -- Albin Tonnerre <lutin@debian.org>  Sun, 07 Sep 2014 13:10:51 +0200

dvtm (0.6-1) unstable; urgency=low

  * New upstream release
  * Switch to dpkg-source 3.0 (quilt) format
  * Bump Standards-Version to 3.9.1
  * debian/control:
    - Update Build-Depends debhelper (>= 7.0.50~)
    - Add Depends on ${misc:Depends}
  * Update debian/rules with new dh format

 -- Xavier Oswald <xoswald@debian.org>  Mon, 18 Oct 2010 15:17:42 +0200

dvtm (0.5.2-2) unstable; urgency=low

  * Recommends: ncurses-term. Fixes cases where dvtm detects that the terminal
    supports 256 colors and tries to use TERM=rxvt-256color (Closes: #543390)
  * Bump Standards-Version to 3.8.3

 -- Albin Tonnerre <albin.tonnerre@gmail.com>  Mon, 24 Aug 2009 21:25:37 +0200

dvtm (0.5.2-1) unstable; urgency=low

  * New upstream release (Closes: #536151)
  * Update Standards-Version to 3.8.2, no change required

 -- Albin Tonnerre <albin.tonnerre@gmail.com>  Wed, 22 Jul 2009 22:42:41 +0200

dvtm (0.5.1-2) unstable; urgency=low

  * debian/control:
    - Remove uneeded DM flag
    - Update Build-Depends debhelper to 7
    - Bump Standards-Version to 3.8.1
    - Change my mail address
  * Update debian/compat to 7
  * Fix lintian warning:
    - W: dvtm source: dh-clean-k-is-deprecated
    - I: dvtm source: quilt-patch-missing-description
      cflags_and_strip_fixes.diff

 -- Xavier Oswald <xoswald@debian.org>  Wed, 29 Apr 2009 00:27:18 +0200

dvtm (0.5.1-1) unstable; urgency=low

  * New upstream release

  [Albin Tonnerre]
  * refresh debian/patches/cflags_and_strip_fixes.diff
    + Add -DCONFIG_CMDFIFO to enable the fifo command interface
  * debian/rules: use /usr/share/quilt/quilt.make instead of our own rules

 -- Xavier Oswald <x.oswald@free.fr>  Tue, 17 Feb 2009 21:55:25 +0100

dvtm (0.5-1) unstable; urgency=low

  [Albin Tonnerre]
  * New upstream release (Closes: #506155, #512544)
  * Update Standards-Version to 3.8.0
  * debian/copyright:
    - Adjust year numbers to 2009 for dvtm and debian/
    - [lintian] fix I: copyright-with-old-dh-make-debian-copyright

  [Xavier Oswald]
  * debian/control: change XS-Dm-Upload-Allowed to Dm-Upload-Allowed

 -- Xavier Oswald <x.oswald@free.fr>  Tue, 27 Jan 2009 00:49:54 +0100

dvtm (0.4.1-1) unstable; urgency=low
  [Albin Tonnerre]
  * New Upstream Version (Closes: #482036)
  * debian/patches/cflags_and_strip_fixes.diff:
    - Explicitely define LDFLAGS = , not +=, as this leads to have both
      -lncurses and -lncursesw appended when you call 'make unicode'

 -- Xavier Oswald <x.oswald@free.fr>  Sun, 25 May 2008 12:55:52 +0200

dvtm (0.4-1) unstable; urgency=low

  * Initial release (Closes: #456599)
  * debian/patches/cflags_and_strip_fixes.diff:
    - Allow passing custom CFLAGS (eg when DEB_BUILD_OPTIONS=noopt)
    - Don't strip the binary in Makefile, let dh_strip handle it

 -- Albin Tonnerre <albin.tonnerre@gmail.com>  Tue, 26 Feb 2008 19:03:36 +0100
